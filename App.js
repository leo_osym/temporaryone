import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    }
  }

  onChangeText = (text) => {
    this.setState({ text })
  }

  render() {
    const { text } = this.state;
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          value={text}
          onChangeText={this.onChangeText}
          textContentType='oneTimeCode'
        />
        <Text style={styles.welcome}>Example for processing messages</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textInput: {
    width: '80%',
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    fontSize: 25
  }
});
